**Anggota Kelompok:**
- Basyira Sabita - 1906400034
- Andrew Nehemia H / 1906400311
- Firman Edo Junyet / 1906399921
- Hafiz Bhadrika Alamsyah / 1906399650
- Reno Fathoni / 1906399461


**Link persona**
https://www.figma.com/file/P7lkkp3x4rAf4U4VeVvCCo/Tugas-1?node-id=2%3A0

**Link wireframe**
https://www.figma.com/file/P7lkkp3x4rAf4U4VeVvCCo/Tugas-1?node-id=0%3A1

**Link Prototype**
https://www.figma.com/file/P7lkkp3x4rAf4U4VeVvCCo/Tugas-1?node-id=1%3A2

Pada TK1 kali ini kelompok kami membuat web mengenai covid-19 dimana didalam web ini
pengguna dapat mendapatkan info-info mengenai Covid-19. Terdapat beberapa fitur pada
inforona yaitu: 1. fitur beranda, 2.  Covid-19: penjelasan mengenai covid-19
3.Surat: informasi surat resmi pemerintah dan kebijakan pemerintah pada pandemi
covid-19, 4. FAQ & Tanya Jawab, 5.informasi: mengenai apa saja yang dapat user lakukan
ketika pandemi covid-19

Deadline 1 nih
# link pipeline
[![pipeline status](https://gitlab.com/basyira.sabita/tk1-ppw/badges/master/pipeline.svg)](https://gitlab.com/basyira.sabita/tk1-ppw/-/commits/master)
[![coverage report](https://gitlab.com/basyira.sabita/tk2-ppw/badges/master/coverage.svg)](https://gitlab.com/basyira.sabita/tk1-ppw/-/commits/master)