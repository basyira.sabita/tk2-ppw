from django.test import TestCase, Client
from .models import AngkaCorona
from .views import apaItuCorona, formAngkaCorona

# Create your tests here.
class Test_apa_itu_corona(TestCase):
    def test_url_apa_itu_corona(self):
        response = Client().get('/apa-itu-corona/')
        self.assertEquals(200, response.status_code)

    def test_url_formAngkaCorona(self):
        response = Client().get('/apa-itu-corona/edit-form-angka-corona')
        self.assertEquals(200, response.status_code)

    def test_views_apa_itu_corona(self):
        response = Client().get('/apa-itu-corona/')
        html = response.content.decode('utf-8')
        self.assertIn("<h1>CARA PENYEBARAN</h1>", html)

    def test_views_formAngkaCorona(self):
        response = Client().get('/apa-itu-corona/edit-form-angka-corona')
        html = response.content.decode('utf-8')
        self.assertIn('<input type="submit" value="Submit" class="btn btn-primary click">', html)

    def test_template_apa_itu_corona(self):
        response = Client().get('/apa-itu-corona/')
        self.assertTemplateUsed(response, 'apa_itu_corona.html')

    def test_template_formAngka(self):
        response = Client().get('/apa-itu-corona/edit-form-angka-corona')
        self.assertTemplateUsed(response, 'formAngka.html')

    def test_model_form_angkaCorona(self):
        AngkaCorona.objects.create(terkonfirmasi="a",perubahan="a",kasusAktif="a",persenAktif="a", sembuh="a", persenSembuh="a", meninggal="a", persenMeninggal="a", tanggal="a")
        jumlah = AngkaCorona.objects.all().count()
        self.assertEquals(jumlah, 1)
    
    def test_url_getData(self):
        response = Client().get('/apa-itu-corona/getData')
        self.assertEquals(200, response.status_code)

    #test post masih error
    # def test_post_formAngka(self):
    #     form = AngkaCorona.objects.create(terkonfirmasi="a",perubahan="a",kasusAktif="a",persenAktif="a", sembuh="a", persenSembuh="a", meninggal="a", persenMeninggal="a", tanggal="a")
    #     response = Client().post('/apa-itu-corona/edit-form-angka-corona', form)
    #     html = response.content.decode('utf-8')
    #     self.assertIn('<h4 style="background-color: #C4C4C4;">a</h4>', html)