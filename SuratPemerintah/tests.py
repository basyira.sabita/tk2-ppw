from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import searchbook, showpdf, list_pdf, upload
from .models import SuratPemerintah
from .apps import SuratpemerintahConfig

# Create your tests here.

class TestEksistensiUrl(TestCase):
    def test_url_SuratPemerintah(self):
        response = Client().get('/SuratPemerintah/')
        self.assertEquals(200, response.status_code)

    def test_url_upload_SuratPemerintah(self):
        response = Client().get("/SuratPemerintah/upload/")
        self.assertEquals(200, response.status_code)


class TestTemplate(TestCase):
    def test_template_SuratPemerintah(self):
        response = Client().get('/SuratPemerintah/')
        self.assertTemplateUsed(response, 'home/home.html')

    def test_template_upload_SuratPemerintah(self):
        response = Client().get('/SuratPemerintah/upload/')
        self.assertTemplateUsed(response, 'home/upload.html')



class TestFungsi(TestCase):
    def test_event_index_func(self):
        found = resolve('/SuratPemerintah/upload/')
        self.assertEqual(found.func, upload)

    def test_event_func(self):
        found_func = resolve('/SuratPemerintah/')
        self.assertEqual(found_func.func, list_pdf)



class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(SuratpemerintahConfig.name, "SuratPemerintah")



class TestSearchEvent(TestCase):
    def test_event_searchbook_url_is_exist(self):
        response = Client().get('/SuratPemerintah/data/?q=book')
        self.assertEqual(response.status_code, 200)

    def test_search_is_exist(self):
        response = Client().get('/SuratPemerintah/')
        self.assertContains(response, 'container-search')


