from django.db import models

# Create your models here.
class Poll(models.Model):
    pertanyaan = models.TextField()
    pilihan_satu = models.CharField(max_length=50)
    pilihan_dua = models.CharField(max_length=50)
    pilihan_tiga = models.CharField(max_length=50)
    pilihan_satu_hitung = models.IntegerField(default=0)
    pilihan_dua_hitung = models.IntegerField(default=0)
    pilihan_tiga_hitung = models.IntegerField(default=0)

    def total(self):
        return self.pilihan_satu_hitung + self.pilihan_dua_hitung + self.pilihan_tiga_hitung

