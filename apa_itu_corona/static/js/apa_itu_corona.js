$('#keyword').keyup( function() {
    var value_keyword = $("#keyword").val();
    console.log(value_keyword);
    // var url_keyword = 'https://dekontaminasi.com/api/id/covid19/hospitals';
    // console.log(url_keyword);
    $.ajax({
        method : 'GET',
        url : '/apa-itu-corona/getData',
        success : function(response) {
            console.log(response);

            var obj_hasil = response;

            var obj_data = $('#data');
            obj_data.empty();

            var table = "";
            for (i = 0; i < obj_hasil.length; i++) {
                var provinsi = obj_hasil[i]["province"];
                //console.log(provinsi)
                if (value_keyword == "") {
                    table += "";
                }
                else if (provinsi.toLowerCase().includes(value_keyword.toLowerCase())) {
                    var nama = obj_hasil[i]["name"];
                    var alamat = obj_hasil[i]["address"];
                    var telpon = obj_hasil[i]["phone"];
                    table += ('<tr>'+ '<td>'+ provinsi + '</td>'+'<td>'+ nama +'</td>' + '<td>'+ alamat +'</td>'+'<td>');
                    if (telpon == null) {
                        table += ( '- </td>'+'</tr>');
                    } else {
                        table += (telpon +'</td>'+'</tr>');
                    }
                    
                }
            }
            obj_data.append(table);
        }
    });
});

$(function() {
    $("#terkonfirmasi").hover(function(){
        $(this).css("background-color", "white");
    }, function() {
        $(this).css("background-color", "#E5F7D1");
    });
    $("#aktif").hover(function(){
        $(this).css("background-color", "white");
    }, function() {
        $(this).css("background-color", "#E5F7D1");
    });
    $("#sembuh").hover(function(){
        $(this).css("background-color", "white");
    }, function() {
        $(this).css("background-color", "#E5F7D1");
    });
    $("#meninggal").hover(function(){
        $(this).css("background-color", "white");
    }, function() {
        $(this).css("background-color", "#E5F7D1");
    });
});

$("#top").on('click', function(){
    $('html, body').animate({scrollTop : 0});
});