from django.urls import path
from .views import index1, addQuestion, filter_search, data
from . import views
from django.conf.urls.static import static
app_name = "FaQ"

urlpatterns = [
    path('FaQ/', views.addQuestion, name='index2'),
    path('tanyajawab/', views.index1, name='index1'),
    path('filter_serach/', views.filter_search, name='index3'),
    path('data/', views.data, name='data'),
]