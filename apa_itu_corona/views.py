from django.shortcuts import render, redirect
from django.http import JsonResponse
from .models import AngkaCorona
from .forms import FormAngkaCorona
import requests, json

# Create your views here.
def apaItuCorona(request):
    dataAngka = AngkaCorona.objects.all()
    return render(request, 'apa_itu_corona.html', {'dataAngka':dataAngka})

def formAngkaCorona(request):
    form = FormAngkaCorona(request.POST or None)
    if form.is_valid() and request.method == "POST" :
        form.save()
        return redirect('/')
    else :
        return render(request, 'formAngka.html', {'form':form})


def get_data_rs(request):
    url = 'https://dekontaminasi.com/api/id/covid19/hospitals/'
    response = requests.get(url)
    data = json.loads(response.content)
    #print(data)
    return JsonResponse(data, safe=False)