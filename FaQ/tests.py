from django.test import TestCase, tag, Client
from django.urls import reverse, resolve
from .views import addQuestion, index1, filter_search
from .models import Ask
from django.apps import apps
from . apps import FaqConfig


class FaQTest(TestCase):
    def test_url_exist(self):
        response = self.client.get(reverse('FaQ:index2'))
        self.assertEqual(response.status_code, 200)
        #untuk cek url bisa atau belum

    def test_landing_page_func(self):
        found = resolve('/tanya/FaQ/')
        self.assertEqual(found.func, addQuestion)
        #untuk test views yang dipake

    def test_used_template(self):
        response = self.client.get(reverse('FaQ:index2'))
        self.assertTemplateUsed(response, 'ques_answ.html')

    def test_create_Ask_object(self):
        Ask.objects.create(
            nama = 'Lontong',
            qoa = 'apa itu covid?',
        )
        count = Ask.objects.all().count()
        self.assertEqual(count, 1)
#buat yg kedua
    def test_url_exist_faq(self):
        response = self.client.get(reverse('FaQ:index1'))
        self.assertEqual(response.status_code, 200)
        #untuk cek url bisa atau belum

    def test_landing_page_func_faq(self):
        found = resolve('/tanya/tanyajawab/')
        self.assertEqual(found.func, index1)
        #untuk test views yang dipake

    def test_used_template_faq(self):
        response = self.client.get(reverse('FaQ:index1'))
        self.assertTemplateUsed(response, 'index2.html')
#untuk filter.html
    def test_url_exist_faq(self):
        response = self.client.get(reverse('FaQ:index3'))
        self.assertEqual(response.status_code, 200)
        #untuk cek url bisa atau belum

    def test_landing_page_func_faq(self):
        found = resolve('/tanya/filter_serach/')
        self.assertEqual(found.func, filter_search)
        #untuk test views yang dipake

    def test_used_template_faq(self):
        response = self.client.get(reverse('FaQ:index3'))
        self.assertTemplateUsed(response, 'filter.html')

    def test_apps(self):
        self.assertEqual(FaqConfig.name, 'FaQ') 
        self.assertEqual(apps.get_app_config('FaQ').name, 'FaQ')

    def test_template(self):
        response = Client().get(reverse("FaQ:index2"))
        self.assertTemplateUsed(response, 'ques_answ.html')

    def test_template2(self):
        response = Client().get(reverse("FaQ:index1"))
        self.assertTemplateUsed(response, 'index2.html')

    def test_template3(self):
        response = Client().get(reverse("FaQ:index3"))
        self.assertTemplateUsed(response, 'filter.html')

    def testConfig(self):
        self.assertEqual(FaqConfig.name, 'FaQ')
    