# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import loginpage, register, logoutpage, index, formAngkaCorona
from django.contrib.auth.models import User


# Create your tests here.
from .apps import HomeConfig

class TestPages(TestCase):
    def test_url_home(self):
        response = Client().get('/')
        self.assertEquals(200, response.status_code)

    def test_app_name(self):
        self.assertEqual(HomeConfig.name, "home")

#LOG OUT
class TestLogOut(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_event_func(self):
        found_func = resolve('/logout/')
        self.assertEqual(found_func.func, logoutpage)



# Register
class TestRegister(TestCase):
    def test_url_home(self):
        response = Client().get('/register/')
        self.assertEquals(200, response.status_code)

    def test_event_func(self):
        found_func = resolve('/register/')
        self.assertEqual(found_func.func, register)

# LOG IN
class TestLogIn(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_event_func(self):
        found_func = resolve('/login/')
        self.assertEqual(found_func.func, loginpage)

