$("#SearchBox").focus(function(){
    $(this).css("background-color", "#ABB868");
});
$("#SearchBox").blur(function(){
    $(this).css("background-color", "white");
});

$("#SearchBox").keyup(function () {
var isi = $("#SearchBox").val();
var url_google_apis = "/SuratPemerintah/data?q=" + isi;
$.ajax({
    url: url_google_apis,
    success: function (hasil) {
        var obj_hasil = $("#Output");
        obj_hasil.empty();

        for (index = 0; index < hasil.items.length; index++) {
            var tmp_title = hasil.items[index].volumeInfo.title;
            var tmp_image = hasil.items[index].volumeInfo.imageLinks.smallThumbnail;
            var tmp_description = hasil.items[index].volumeInfo.description;
            var tmp_more_on = hasil.items[index].volumeInfo.previewLink;
            var final_image = "<img src=" + tmp_image + "class=" + "card-img" + ">"
            obj_hasil.append(
                // "<div class=" + "card mb-3" + "style=" + "max-width: 540px;" + ">" +
                //     "<div class=" + "row no-gutters" + ">" +
                //         "<div class=" + "col-md-4" + " style='margin: auto; display: flex'>" +
                //             final_image +
                //         "</div>" +
                //         "<div class=" + "col-md-8" + ">" +
                //             "<div class=" + "card-body" + ">" +
                //                 "<h5 class=" + "card-title" + ">" + tmp_title + "</h5>" +
                //                 "<p class=" + "card-text" +">" + tmp_description + "</p>" +
                //                 "<a target='_blank' href=" + tmp_more_on + "class=" + "btn btn-primary" + " style='color: black;'>" + "Read More" + "</a>" +
                //             "</div>" +
                //         "</div>" +
                //     "</div>" +
                // "</div>" + #1e7e34
                
                "<div class=" + "card" + " style='margin-bottom:10px;'" + ">" +
                    "<ul class=" + "list-group" +  ">" +
                    "<li class=" + "list-group-item" + ">" +
                        "<div" + ">" +
                            "<div class=" + " style=' display: flex;'" + ">" +
                                final_image +
                            "</div>" +
                            "<div class=" + "card-body" + ">" + tmp_title + "</div>" + 
                            "<p class=" + "card-text" +">" + tmp_description + "</p>" +
                            "<a target='_blank ' href=" + tmp_more_on + "class=" + "card-link" + ">" + "Read More" + "</a>" +
                        "</div>" +
                    "</li>" +
                    "</ul>" +
                "</div>"

                );
            }
        }
    });
});


$("#ubah").click(function () {
    if ($(this).hasClass("gayed")) {
        $(this).removeClass("gayed").text("Mencari Buku Tentang Corona?");
    } else {
        $(this).addClass("gayed").text("Ketik Judul Buku yang kamu inginkan..");
    }
})

$("#ubah").hover(function(){
    $(this).css("color", "#ABB868");
    }, function(){
    $(this).css("color", "#212529");
});

$(".changetext1").click(function () {
    if ($(this).hasClass("gayed1")) {
        $(this).removeClass("gayed1").text("Peraturan Pemerintah Pengganti Undang-Undang");
    } else {
        $(this).addClass("gayed1").text("(PERPU)");
    }
})

$(".changetext2").click(function () {
    if ($(this).hasClass("gayed2")) {
        $(this).removeClass("gayed2").text("Undang-Undang");
    } else {
        $(this).addClass("gayed2").text("(UU)");
    }
})

$(".changetext3").click(function () {
    if ($(this).hasClass("gayed3")) {
        $(this).removeClass("gayed3").text("Peraturan Pemerintah");
    } else {
        $(this).addClass("gayed3").text("(pp)");
    }
})

$(".changetext4").click(function () {
    if ($(this).hasClass("gayed4")) {
        $(this).removeClass("gayed4").text("Peraturan Presiden");
    } else {
        $(this).addClass("gayed4").text("(PERPRES)");
    }
})

$(".changetext5").click(function () {
    if ($(this).hasClass("gayed5")) {
        $(this).removeClass("gayed5").text("Peraturan Daerah");
    } else {
        $(this).addClass("gayed5").text("(PERDA)");
    }
})

$(".changetext6").click(function () {
    if ($(this).hasClass("gayed6")) {
        $(this).removeClass("gayed6").text("Peraturan Gubernur");
    } else {
        $(this).addClass("gayed6").text("(PERGUB)");
    }
})

$("#pencet").hover(function(){
    $(this).css("color", "white");
    }, function(){
    $(this).css("color", "#212529");
});

// $(".changetext").mouseenter(function(){
//     alert("OOOPS!!, Coba klik tulisan KEBIJAKAN PEMERINTAH");
// });

$("#pencet").hover(function(){
    $(this).css("color", "white");
    }, function(){
    $(this).css("color", "#212529");
});

$("#pencet2").hover(function(){
    $(this).css("color", "white");
    }, function(){
    $(this).css("color", "#212529");

});

$("#pencet3").hover(function(){
    $(this).css("color", "white");
    }, function(){
    $(this).css("color", "#212529");

});

$("#pencet4").hover(function(){
    $(this).css("color", "white");
    }, function(){
    $(this).css("color", "#212529");

});

$("#pencet5").hover(function(){
    $(this).css("color", "white");
    }, function(){
    $(this).css("color", "#212529");

});

$("#pencet6").hover(function(){
    $(this).css("color", "white");
    }, function(){
    $(this).css("color", "#212529");

});




// $(".reorder-up").click(function () {
//     var $current = $(this).closest('.card')
//     var $previous = $current.prev('.card');
//     if ($previous.length !== 0) {
//         $current.delay(100000).insertBefore($previous);
//     }
//     return false;
// });

// <label class="container">Two
//                 <input type="checkbox">
//                 <span class="checkmark"></span>
//               </label>


// $(".reorder-down").click(function () {
//     var $current = $(this).closest('.card')
//     var $next = $current.next('.card');
//     if ($next.length !== 0) {
//         $current.insertAfter($next);
//     }
//     return false;
// });