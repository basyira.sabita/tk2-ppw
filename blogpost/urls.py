from django.urls import path
from . import views
from .views import BlogView, BlogDetailView, BlogAddView
from django.views.decorators.csrf import csrf_exempt

appname = 'blogpost'

urlpatterns = [
    path('', BlogView.as_view(), name='blog'),
    path('article/<int:pk>/', BlogDetailView.as_view(), name="blog-detail"),
    path('post/', BlogAddView.as_view(), name='post'),
    path('cari', csrf_exempt(views.cari), name="cari")
]