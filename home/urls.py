from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('edit-form-angka-corona', views.formAngkaCorona),
    path('login/', views.loginpage, name='loginpage'),
    path('logout/', views.logoutpage, name='logoutpage'),
    path('register/', views.register, name='register'),
    path('index/', views.index)
]