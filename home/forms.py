from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import AngkaCorona

class FormAngkaCorona(ModelForm):
    def __init__ (self, *args, **kwargs) :
        super().__init__(*args, **kwargs)
        for _, value in self.fields.items():
            value.widget.attrs['placeholder'] = value.help_text
    
    class Meta:
        model = AngkaCorona
        fields = '__all__'

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields= ['username', 'email', 'password1', 'password2']