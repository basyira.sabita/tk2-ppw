from django.urls import path
from apa_itu_corona.views import apaItuCorona, formAngkaCorona, get_data_rs

urlpatterns = [
    path('', apaItuCorona, name="apaItuCorona"),
    path('edit-form-angka-corona', formAngkaCorona),
    path('getData', get_data_rs, name='data'),
]