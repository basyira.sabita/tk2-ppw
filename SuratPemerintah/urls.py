from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'SuratPemerintah'

urlpatterns = [
    path('upload/',views.upload, name="upload"),
    path('',views.list_pdf, name="list_pdf"),
    path('<int:idpdf>/',views.showpdf, name="showpdf"),
    path('data/', views.searchbook, name='searchbook'),
    # path('perpu/',views.perpu, name="perpu"),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)