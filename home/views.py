from django.shortcuts import render, redirect
from .models import AngkaCorona
from .forms import FormAngkaCorona
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout
# Create your views here.

def index(request):
    dataAngka = AngkaCorona.objects.all()
    return render(request, 'home/index.html', {"dataAngka":dataAngka})

def formAngkaCorona(request):
    form = FormAngkaCorona(request.POST or None)
    if form.is_valid() and request.method == "POST" :
        form.save()
        return redirect('/')
    else :
        return render(request, 'formAngka.html', {'form':form})

def loginpage(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/')

    return render(request,'home/login.html')

def logoutpage(request) :
    logout(request)
    return redirect('/')

def register(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/login')
    context = {'form' : form}
    return render(request,'home/register.html', context)