from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView
from .models import BlogPost
from .forms import PostForm
import json
from django.http import JsonResponse
from django.http import HttpResponse


class BlogView(ListView):
    model = BlogPost
    template_name = "blog.html"

class BlogDetailView(DetailView):
    model = BlogPost
    template_name = "blog-detail.html"

class BlogAddView(CreateView):
    model = BlogPost
    form_class = PostForm
    template_name = 'blog-add.html'

def cari(request):
    if request.method == "POST":
        search_str = json.loads(request.body).get('searchText')
        blogs = BlogPost.objects.filter(title__icontains=search_str) | BlogPost.objects.filter(writer__icontains=search_str)
        data = blogs.values()
        return JsonResponse(list(data), safe=False)
