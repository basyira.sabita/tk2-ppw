from django.contrib import admin

# Register your models here.
from .models import SuratPemerintah

admin.site.register(SuratPemerintah)