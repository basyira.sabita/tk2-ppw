from django.apps import AppConfig


class SuratpemerintahConfig(AppConfig):
    name = 'SuratPemerintah'
