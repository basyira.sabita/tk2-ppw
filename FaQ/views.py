from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .models import Ask
import requests
import json

def index1(request):
    return render(request, 'index2.html')

def filter_search(request):
    response = {}
    return render(request, 'filter.html', response)

def addQuestion(request):
    Parameter = False
    nama = ""
    question = Ask.objects.all()

    if request.method == 'POST':
        Parameter = True
        name = request.POST.get("nama")
        pertanyaan = request.POST.get("Pertanyaan")
        q = Ask.objects.create(nama = name, qoa = pertanyaan)
        q.save()
        return redirect("/tanya/FaQ")

    context = {
        'param' : Parameter,
        'answer' : question
    }
    return render(request, 'ques_answ.html', context)

def data(request):
    arg = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url_tujuan)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)