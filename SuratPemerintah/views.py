from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView, CreateView
from django.core.files.storage import FileSystemStorage
from django.urls import reverse_lazy
from .forms import SuratPemerintahForm
from .models import SuratPemerintah

from django.http import JsonResponse
from django.http import HttpResponse
import requests
import json

# Create your views here.

def upload(request):
    #form = SuratPemerintah()
    if request.method == 'POST':
        form = SuratPemerintahForm(request.POST, request.FILES)
        form.save()
        return redirect('../')
    else:
        form = SuratPemerintahForm()

    return render(request, 'home/upload.html',{'form':form})

def list_pdf(request):
    if request.method == 'POST':
        form = SuratPemerintahForm(request.POST, request.FILES)
        form.save()
        return redirect('/SuratPemerintah')
    else:
        form = SuratPemerintahForm()

    suratPemerintah = SuratPemerintah.objects.all()
    return render(request, 'home/home.html',{ 'suratPemerintah':suratPemerintah,"form":form })

def showpdf(request,idpdf):
    lihatpdf = SuratPemerintah.objects.get(id = idpdf)
    return render(request,'home/ShowPdf.html',{'lihatpdf' : lihatpdf})

def searchbook(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    retrive = requests.get(url)
    data = json.loads(retrive.content)
    return JsonResponse(data, safe=False)
